import 'package:flutter/material.dart';

import '../Constants.dart';

class GeneralDialogs {

  static Future showalertdialog(
      BuildContext context,
      {
    @required String title,
    String subTitle,
    String buttonText = "Done",
    Color buttonColor,
    Function buttonAction,
  }){
    return showDialog(context: context,
        builder: (context){
      return Dialog(
        backgroundColor: Colors.white,
        elevation: 10,
        child: Container(
          padding: EdgeInsets.fromLTRB(8, 16, 8, 32),
          height: 300,
          width: MediaQuery.of(context).size.width -20,
          child: Column(
            children: [
              SizedBox(height: 35,),
              Text("$title", style:AppConstants.bodyFont
                  .apply(color: AppConstants.primaryColor, fontWeightDelta: 2, fontSizeFactor: 1.6),textAlign: TextAlign.center
              ),
              if(subTitle!=null)
                SizedBox(height: 35,),
              Text("$subTitle",style:AppConstants.bodyFont
                  .apply(color: AppConstants.primaryColor, fontWeightDelta: 2, fontSizeFactor: 1.2),textAlign: TextAlign.center,),
              // Text("Successful!",style:AppConstants.bodyFont
              //     .apply(color: AppConstants.primaryColor, fontWeightDelta: 2, fontSizeFactor: 1.2),textAlign: TextAlign.center),
              Spacer(),
              Container(
                width: double.maxFinite,
                child: TextButton(
                  onPressed: () async{

                      await Navigator.pop(context);


                  },
                  child: Text(
                    "${buttonText}",
                    style: AppConstants.bodyFont
                        .apply(color: buttonColor==null?Colors.white:buttonColor, fontWeightDelta: 2, fontSizeFactor: 0.8),
                      textAlign: TextAlign.center,
                  ),
                  style: TextButton.styleFrom(
                      primary: AppConstants.primaryColor,
                      backgroundColor: AppConstants.primaryColor,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)
                      )
                  ),
                ),
              ),
            ],
          ),
        ),
      );
        }
    );
  }

}

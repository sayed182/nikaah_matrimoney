import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppConstants{

  static TextStyle bodyFont = GoogleFonts.josefinSans(
    color: Colors.black,
    fontWeight: FontWeight.w700,
    fontSize: 16,
    letterSpacing: 1,
  );

  static TextStyle fontLabelFont = GoogleFonts.josefinSans(
    color: Colors.white,
    fontWeight: FontWeight.w700,
    fontSize: 16,
    letterSpacing: 1,
  );

  static Color primaryColor = Color(0xFFFC4265);

  static InputDecoration formFieldDecoration = InputDecoration(
    contentPadding: EdgeInsets.all(10),
    border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(18.0),
        borderSide: BorderSide(color: Colors.white)
    ),
    enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(18.0),
        borderSide: BorderSide(color: Colors.white)
    ),
    focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(18.0),
        borderSide: BorderSide(color: Colors.white)
    ),
    disabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(18.0),
        borderSide: BorderSide(color: Colors.white)
    ),
  );

  static const Gradient backgroundGradient = LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: [
        Color(0xFFFF3E59),
        Color(0xFFF32068),
        Color(0xFFF30072),
      ],
      stops: [
        0.5,
        0.9,
        1.0
      ]
  );
}
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:matrimony_ui/Constants.dart';
import 'package:matrimony_ui/Dashboard/add_photo.dart';
import 'package:matrimony_ui/Dashboard/dashboard.dart';
import 'package:matrimony_ui/Partner_Preference/partner_preference_screen.dart';
import 'package:matrimony_ui/Registration/registration.dart';

import 'Partner_Preference/partner_preference_second_screen.dart';
import 'Partner_Preference/partner_preferences_third_screen.dart';
import 'Registration/registration_fifth_screen.dart';
import 'Registration/registration_fourth_screen.dart';
import 'Registration/registration_second_screen.dart';
import 'Registration/registration_third_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
        textTheme: TextTheme(
          bodyText1: AppConstants.fontLabelFont,
          bodyText2: AppConstants.bodyFont,
          button: AppConstants.fontLabelFont,
        )
      ),
      home: LoginPage(),
      initialRoute: Dashboard.routeName,
      routes: {
        LoginPage.routeName : (context) => LoginPage(),
        RegistrationPage.routeName : (context) => RegistrationPage(),
        RegistrationSecondPage.routeName : (context) => RegistrationSecondPage(),
        RegistrationThirdPage.routeName : (context) => RegistrationThirdPage(),
        RegistrationFourthPage.routeName : (context) => RegistrationFourthPage(),
        RegistrationFifthPage.routeName : (context) => RegistrationFifthPage(),
        PartnerPreferenceScreen.routeName : (context) => PartnerPreferenceScreen(),
        PartnerPreferenceSecondScreen.routeName : (context) => PartnerPreferenceSecondScreen(),
        PartnerPreferencesThirdScreen.routeName : (context) => PartnerPreferencesThirdScreen(),
        Dashboard.routeName : (context) => Dashboard(),
        AddPhoto.routeName : (context) => AddPhoto(),
      },
    );
  }
}

class LoginPage extends StatefulWidget {
  static const routeName = 'login';
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 50),
          width: _size.width,
          decoration: BoxDecoration(
            gradient: AppConstants.backgroundGradient,
          ),
          child: Column(
            children: <Widget>[
              Spacer(),
              Text(
                'Lets Nikah',
                style: GoogleFonts.pacifico(fontWeight: FontWeight.w400, fontSize: 60.0, color: Colors.white),
              ),
              SizedBox(height: 55,),
              TextButton(
                onPressed: (){
                  Navigator.pushNamed(context, RegistrationPage.routeName);
                },
                style: TextButton.styleFrom(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  primary: Colors.white,
                  backgroundColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0)
                  )
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(width: 30,),
                    SvgPicture.asset('assets/icons/facebook_icon.svg', height: 28,),
                    SizedBox(width: 30,),
                    Text("Sign in with facebook".toUpperCase(),
                      style: AppConstants.bodyFont.apply(color: Color(0xFF3B5998), fontWeightDelta: 2),
                    )
                  ],
                ),
              ),
              SizedBox(height: 15,),
              TextButton(
                style: TextButton.styleFrom(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    primary: Colors.white,
                    backgroundColor: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0)
                    ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(width: 30,),
                    SvgPicture.asset('assets/icons/instagram_icon.svg', height: 28,),
                    SizedBox(width: 30,),
                    Text("Sign in with Instagram".toUpperCase(),
                      style: AppConstants.bodyFont.apply(color: Color(0xFFBC2A8D), fontWeightDelta: 2),
                    )
                  ],
                ),
              ),
              SizedBox(height: 70,),
              Text("By Signing up, you agree to our\nTerms & Conditions & Privacy Policy",
                style: AppConstants.bodyFont.apply(color: Colors.white),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 120,)
            ],
          ),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

import 'package:flutter/material.dart';
import 'package:matrimony_ui/Constants.dart';

class PartnerPreferenceSecondScreen extends StatefulWidget {
  static const routeName = 'partner_preference_second';
  @override
  _PartnerPreferenceSecondScreenState createState() =>
      _PartnerPreferenceSecondScreenState();
}

class _PartnerPreferenceSecondScreenState
    extends State<PartnerPreferenceSecondScreen> {
  int _profileForController;
  DateTime _dateOfBirth;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dateOfBirth = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: Container(
            child: Icon(
          Icons.chevron_left,
          color: Colors.white,
        )),
        centerTitle: true,
        title: Text(
          "PARTNER PREFERENCES",
          style: AppConstants.bodyFont.apply(
              color: Colors.white, fontSizeDelta: 2, letterSpacingFactor: 1.4),
        ),
        actions: [
          Center(
            child: InkWell(
              child: Text(
                "SKIP",
                style: AppConstants.bodyFont
                    .apply(color: Colors.white, fontSizeFactor: 0.8),
              ),
            ),
          )
        ],
        backgroundColor: Color(0xFFFF3E59),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.fromLTRB(36, 0, 36, 36),
            width: _size.width,
            constraints: BoxConstraints(minHeight: _size.height),
            decoration:
                BoxDecoration(gradient: AppConstants.backgroundGradient),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                    child: Text(
                  "We will show matches based on your Inputs",
                  style: AppConstants.fontLabelFont,
                )),
                Center(
                  child: Container(
                    margin: EdgeInsets.only(top: 16, bottom: 16),
                    child: Text(
                      "RELIGIOUS PREFERENCES",
                      style: AppConstants.fontLabelFont,
                    ),
                  ),
                ),
                // Center(
                //   child:
                // ),
                // SizedBox(height: 32,),
                Text(
                  "RELIGION",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                              child: Text(label.toString()),
                              value: label,
                            ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),

                Text(
                  "MOTHER TONGUE",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                              child: Text(label.toString()),
                              value: label,
                            ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),

                Text(
                  "CASTE",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                              child: Text(label.toString()),
                              value: label,
                            ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),

                Text(
                  "STAR OR NATCHATHIRAM",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                              child: Text(label.toString()),
                              value: label,
                            ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),

                Text(
                  "MANGLIK",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                              child: Text(label.toString()),
                              value: label,
                            ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),

                Container(
                  width: double.maxFinite,
                  child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, PartnerPreferenceSecondScreen.routeName);
                    },
                    child: Text(
                      "NEXT",
                      style: AppConstants.bodyFont.apply(
                          color: Color(0xFFBC2A8D),
                          fontWeightDelta: 2,
                          fontSizeFactor: 1.6),
                    ),
                    style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.white,
                        padding: EdgeInsets.symmetric(vertical: 10),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15))),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

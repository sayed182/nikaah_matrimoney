import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:matrimony_ui/Constants.dart';

class Dashboard extends StatefulWidget {
  static const routeName = 'dashboard';
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.white,
            Color(0xFFF5E8EA)
          ],
          stops: [
            0.0,
            0.5,
          ]
        )
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          centerTitle: true,
          title: Text(
            'Letsnikah',
            style: GoogleFonts.pacifico(fontWeight: FontWeight.w600, fontSize: 26.0, color: AppConstants.primaryColor),
          ),
          leading: InkWell(
            child: Icon(Icons.menu_outlined, color: AppConstants.primaryColor,size: 36,),
          ),
          actions: [
            InkWell(
              child: Icon(Icons.search, color: AppConstants.primaryColor, size: 36,),
            ),
          ],
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(14.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(16.0),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xFFFD4663),
                          Color(0xFFFF3BB1)
                        ]
                      ),
                      borderRadius: BorderRadius.circular(15)
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Flexible(child: Container(
                              height: 140,
                              width: 140,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                              ),
                                clipBehavior: Clip.hardEdge,
                                child: Image.asset("assets/icons/photo.jpeg"),
                            ),
                            ),
                            SizedBox(width: 15,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Hi! \nJohn Abraham", style: TextStyle(color: Colors.white, fontSize: 14) ),
                                SizedBox(height: 15,),
                                TextButton(onPressed: (){}, child: Text("Upgrade Membership", style: TextStyle(color: Colors.white),),
                                  style: TextButton.styleFrom(
                                      backgroundColor: Color(0xff8850C1),
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(15)
                                      ),
                                      padding: EdgeInsets.fromLTRB(10,16,10,14)
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Your Profile is pending Verification", style: TextStyle(color: Colors.white, fontSize: 14) ),
                            TextButton(onPressed: (){}, child: Text("Verify Now", style: TextStyle(color: Colors.white),),
                            style: TextButton.styleFrom(
                              backgroundColor: Color(0xff8850C1),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15)
                              ),
                              padding: EdgeInsets.fromLTRB(10,16,10,14)
                            ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      DashboardSmallCard(
                        title:"NEW MATCHES",
                        value: "10",
                      ),
                      DashboardSmallCard(
                        title:"SHORLISTED PROFILE",
                        value: "10",
                      ),
                      DashboardSmallCard(
                        title:"VIEWED YOUR PROFILE",
                        value: "10",
                      ),
                      DashboardSmallCard(
                        title:"RESPONSES RECEIVED",
                        value: "10",
                      ),
                    ],
                  ),
                  SizedBox(height: 25,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Recommendations", style: TextStyle(color: Color(0xFFFD4566), fontWeight: FontWeight.w700, fontSize: 24),),
                      Text("View All", style: TextStyle(color: Color(0xFF8850C1), fontWeight: FontWeight.w700, fontSize: 16),),

                    ],
                  ),
                  Container(
                    height: 135,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        DashboardProfileThumb(
                          img: "assets/icons/woman-avatar.png",
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 25,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Premium Members", style: TextStyle(color: Color(0xFFFD4566), fontWeight: FontWeight.w700, fontSize: 24),),
                      Text("View All", style: TextStyle(color: Color(0xFF8850C1), fontWeight: FontWeight.w700, fontSize: 16),),

                    ],
                  ),
                  Container(
                    height: 135,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        DashboardProfileThumb(
                          img: "assets/icons/woman-avatar.png",
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,

          backgroundColor: Color(0xFFF5E8EA),
          // elevation: 10,
          iconSize: 36,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home_outlined,), label: ""),
            BottomNavigationBarItem(icon: Icon(Icons.home_outlined,),  label: ""),
            BottomNavigationBarItem(icon: Icon(Icons.home_outlined,),label: "", ),
          ],
        ),
      ),
    );
  }
}

class DashboardSmallCard extends StatelessWidget {
  final String title;
  final String value;

  const DashboardSmallCard({Key key, this.title, this.value}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        height: 113,
        padding: EdgeInsets.all(12),
        margin: EdgeInsets.only(left: 5, right: 5),
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFFFD4663),
                  Color(0xFFFF3BB1)
                ]
            ),
            borderRadius: BorderRadius.circular(15)
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text("$title", style: TextStyle(fontSize: 10, color: Colors.white), textAlign: TextAlign.center,),
            Text("$value", style: TextStyle(fontSize: 20, color: Colors.white),)
          ],
        ),
      ),
    );
  }
}

class DashboardProfileThumb extends StatelessWidget {
  final String img;
  final String name;

  const DashboardProfileThumb({Key key, this.img, this.name}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      margin: EdgeInsets.only(left: 5, right: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            height: 106,
            width: 106,
            padding: EdgeInsets.all(16),
            margin: EdgeInsets.only(bottom: 10, ),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color(0xFFF87FC1),
                      Color(0xFFCD9CFE)
                    ]
                ),
                borderRadius: BorderRadius.circular(1000)
            ),
            child: Image.asset(img),
          ),
          Text("Anusha", )
        ],
      ),
    );
  }
}


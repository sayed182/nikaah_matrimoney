import 'package:flutter/material.dart';

import '../Constants.dart';

class AddPhoto extends StatefulWidget {
  static const routeName = 'add_photo';
  @override
  _AddPhotoState createState() => _AddPhotoState();
}

class _AddPhotoState extends State<AddPhoto> {
  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(gradient: AppConstants.backgroundGradient),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          leading: Container(
              child:  Icon(Icons.chevron_left, color: Colors.white,)
          ),
          title: Text(
            "Add Photo".toUpperCase(),
            style: AppConstants.bodyFont.apply(
                color: Colors.white, fontSizeDelta: 2, letterSpacingFactor: 1.4),
          ),
          actions: [
            Center(
              child: InkWell(
                child: Text("SKIP", style: AppConstants.bodyFont.apply(color: Colors.white, fontSizeFactor: 0.8),),
              ),
            )
          ],
          backgroundColor: Colors.transparent,
        ),
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.fromLTRB(36, 18, 36, 36),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Upload Photos to your profile to get more responses",
                  style: AppConstants.fontLabelFont,
                ),
                Expanded(child: Container(
                  child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
                    itemCount: 9,
                    itemBuilder: (_, index){
                      return Container(
                      height: 140,
                      width: 140,
                          margin: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                      ),
                      clipBehavior: Clip.hardEdge,
                      child: Image.asset("assets/icons/photo.jpeg"),
                      );
                    },
                  ),
                ),),

                Container(
                  width: double.maxFinite,
                  child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, AddPhoto.routeName);
                    },
                    child: Text(
                      "NEXT",
                      style: AppConstants.bodyFont
                          .apply(color: Color(0xFFBC2A8D), fontWeightDelta: 2, fontSizeFactor: 1.6),
                    ),
                    style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.white,
                        padding: EdgeInsets.symmetric(vertical: 10),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)
                        )
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:matrimony_ui/Constants.dart';
import 'package:matrimony_ui/Registration/registration_second_screen.dart';

class RegistrationPage extends StatefulWidget {
  static const routeName = 'registration';
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  int _profileForController;
  DateTime _dateOfBirth;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dateOfBirth = DateTime.now();
  }
  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Registration".toUpperCase(),
          style: AppConstants.bodyFont.apply(
              color: Colors.white, fontSizeDelta: 2, letterSpacingFactor: 1.4),
        ),
        backgroundColor: Color(0xFFFF3E59),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.fromLTRB(36, 18, 36, 36),
            width: _size.width,
            decoration:
                BoxDecoration(gradient: AppConstants.backgroundGradient),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Profile for".toUpperCase(),
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                              child: Text(label.toString()),
                              value: label,
                            ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),
                Text(
                  "Name".toUpperCase(),
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: TextFormField(
                    decoration: AppConstants.formFieldDecoration,
                  ),
                ),
                Text(
                  "Mobile Number".toUpperCase(),
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: TextFormField(
                    decoration: AppConstants.formFieldDecoration,
                  ),
                ),
                Text(
                  "Email".toUpperCase(),
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: TextFormField(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                  ),
                ),
                Text(
                  "Date of birth".toUpperCase(),
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: TextField(
                    // enabled: false,
                    controller: TextEditingController(text: "${_dateOfBirth.day} / ${_dateOfBirth.month} -/${_dateOfBirth.year}"),
                    style: AppConstants.fontLabelFont,
                    readOnly: true,
                    decoration: AppConstants.formFieldDecoration.copyWith(suffixIcon: Icon(Icons.calendar_today_rounded, color: Colors.white,)),
                    onTap: ()async{
                      print("Tapped");
                      final DateTime picked = await showDatePicker(
                          context: context,
                          initialDate: DateTime(DateTime.now().year- 18),
                          firstDate: DateTime(1990),
                          lastDate: DateTime(DateTime.now().year- 18),
                      );
                      if(picked != null && picked != _dateOfBirth){
                        setState(() {
                          _dateOfBirth = picked;
                        });
                      }
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                    ),
                    // borderRadius: BorderRadius.circular(18)
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          color: Colors.white,
                          padding: EdgeInsets.symmetric(vertical: 18),
                          child: Text(
                            "Male",
                            style: AppConstants.fontLabelFont
                                .apply(color: Color(0xFFFC4364)),
                            textAlign: TextAlign.center,
                            // style: AppConstants.fontLabelFont,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 18),
                          child: Text(
                            "Female",
                            style: AppConstants.fontLabelFont,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.maxFinite,
                  child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, RegistrationSecondPage.routeName);
                    },
                    child: Text(
                      "NEXT",
                      style: AppConstants.bodyFont
                          .apply(color: Color(0xFFBC2A8D), fontWeightDelta: 2, fontSizeFactor: 1.6),
                    ),
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.white,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)
                      )
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:matrimony_ui/Constants.dart';
import 'package:matrimony_ui/Registration/registration_fourth_screen.dart';

class RegistrationThirdPage extends StatefulWidget {
  static const routeName = 'registration_third';
  @override
  _RegistrationThirdPageState createState() => _RegistrationThirdPageState();
}

class _RegistrationThirdPageState extends State<RegistrationThirdPage> {
  int _profileForController;
  DateTime _dateOfBirth;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dateOfBirth = DateTime.now();
  }
  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: Container(
          child:  Icon(Icons.chevron_left, color: Colors.white,)
        ),
        centerTitle: true,
        title: Text(
          "NATIVITY",
          style: AppConstants.bodyFont.apply(
              color: Colors.white, fontSizeDelta: 2, letterSpacingFactor: 1.4),
        ),
        actions: [
          Center(
            child: InkWell(
              child: Text("SKIP", style: AppConstants.bodyFont.apply(color: Colors.white, fontSizeFactor: 0.8),),
            ),
          )
        ],
        backgroundColor: Color(0xFFFF3E59),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.fromLTRB(36, 18, 36, 36),
            width: _size.width,
            decoration:
            BoxDecoration(gradient: AppConstants.backgroundGradient),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Center(
                //   child:
                // ),
                // SizedBox(height: 32,),
                Text(
                  "COUNTRY",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                      child: Text(label.toString()),
                      value: label,
                    ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),
                Text(
                  "STATE",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                      child: Text(label.toString()),
                      value: label,
                    ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),
                Center(
                  child: Container(margin: EdgeInsets.only(top: 16, bottom: 32),child: Text("PRESENT LIVING & WORKING LOCATION",style: AppConstants.fontLabelFont,textAlign: TextAlign.center,)),
                ),
                Text(
                  "COUNTRY",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                      child: Text(label.toString()),
                      value: label,
                    ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),
                Text(
                  "STATE",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                      child: Text(label.toString()),
                      value: label,
                    ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),

                Text(
                  "STATUS",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                      child: Text(label.toString()),
                      value: label,
                    ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),
                Center(
                  child: Container(
                    margin: EdgeInsets.only(top: 16, bottom: 32),
                      child: Text("PROFESSION & EDUCATION", style: AppConstants.fontLabelFont,)),
                ),
                Text(
                  "HIGHEST EDUCATION",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                      child: Text(label.toString()),
                      value: label,
                    ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),



                Text(
                  "UNIVERSITY",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                      child: Text(label.toString()),
                      value: label,
                    ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),
                Text(
                  "EMPLOYMENT",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                      child: Text(label.toString()),
                      value: label,
                    ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),

                Text(
                  "OCCUPATION",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                      child: Text(label.toString()),
                      value: label,
                    ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),
                Text(
                  "ANNUAL INCOME",
                  style: AppConstants.fontLabelFont,
                ),
                Container(
                  margin: EdgeInsets.only(top: 16, bottom: 16),
                  child: DropdownButtonFormField<int>(
                    decoration: AppConstants.formFieldDecoration,
                    style: AppConstants.fontLabelFont,
                    value: _profileForController,
                    items: [1, 2, 3, 4, 5]
                        .map((label) => DropdownMenuItem(
                      child: Text(label.toString()),
                      value: label,
                    ))
                        .toList(),
                    hint: Text(' '),
                    onChanged: (value) {
                      setState(() {
                        _profileForController = value;
                      });
                    },
                  ),
                ),


                Container(
                  width: double.maxFinite,
                  child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, RegistrationFourthPage.routeName);
                    },
                    child: Text(
                      "NEXT",
                      style: AppConstants.bodyFont
                          .apply(color: Color(0xFFBC2A8D), fontWeightDelta: 2, fontSizeFactor: 1.6),
                    ),
                    style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.white,
                        padding: EdgeInsets.symmetric(vertical: 10),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)
                        )
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
